from django.urls import path
from .Views import forum, forumFunc
from django.conf.urls import url

app_name = 'forumapp'

urlpatterns = [
    url(r'^main$', forum.Forum.as_view(), name="Home"),
    url(r'^postpost$', forumFunc.postPost, name="PostPost"),
    url(r'^getposts$', forumFunc.getPosts, name="GetPosts"),
]
