from django.contrib import admin
from forumapp.models import *

# Register your models here.
admin.site.register(Post)
admin.site.register(Reply)
