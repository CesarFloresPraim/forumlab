from django.core.exceptions import ValidationError
from django.shortcuts import render, redirect
from django.views.generic import TemplateView
from django.http import HttpResponse, JsonResponse
from forumapp.models import Post, Reply
import logging
import json


def postPost(request):
    action = request.POST.get('action', False)
    type = request.POST.get('type', False)

    if type == 'post':
        if action == 'edit':
            post = Post.objects.get(pk=request.POST['id'])
            post.text = request.POST.get('text', False)
            post.full_clean()
            post.save()
        elif action == 'reply':
            reply = Reply(text=request.POST.get('text', False), replies_id=request.POST['id'])
            reply.full_clean()
            reply.save()
        elif action == 'delete':
            Post.objects.filter(pk=request.POST['id']).delete()
    elif type == 'rep':
        if action == 'edit':
            post = Reply.objects.get(pk=request.POST['id'])
            post.text = request.POST.get('text', False)
            post.full_clean()
            post.save()
        elif action == 'delete':
            Reply.objects.filter(pk=request.POST['id']).delete()
    else:
        post = Post(text=request.POST.get('text', False))
        post.full_clean()
        post.save()

    return HttpResponse("Success", content_type='text/html')


def getPosts(request):
    posts = Post.objects.all()
    posts_html = """
               <div class="card text-center">
                   <div class="card-header">
                       Posts
                   </div>
       """

    for pst in posts:
        posts_html += """
           <div class="card-body">
                   <div class="card border-secondary mb-3">
                       <div class="card-header"><i class="fa fa-user"></i> User</div>
                       <div class="card-body text-secondary">
                           <div class="alert alert-secondary" role="alert">
                               {0}
                           </div>
                           <button id="edit-post-{1}" type="button" class="btn btn-info"><i class="fa fa-pencil"></i></button>
                           <button id="delete-post-{1}" type="button" class="btn btn-danger"><i class="fa fa-trash"></i></button>

                           <hr/>
           """.format(pst.text, pst.id)
        for rep in pst.reply_set.all():
            posts_html += """
               <div>
                   <i class="fa fa-user"></i> User<p class="card-text ml-3">{0}</p>
               </div>
               <button id="edit-rep-{1}" type="button" class="btn btn-info"><i class="fa fa-pencil"></i></button>
               <button id="delete-rep-{1}" type="button" class="btn btn-danger"><i class="fa fa-trash"></i></button>
               """.format(rep.text, rep.id)
        posts_html += """
                                   </div>
                       </div>
                   </div>
           """
    posts_html += """
                       <div class="card-footer text-muted">
                       Loaded a min ago ...
                   </div>
               </div>
       """

    args = {
        'posts_html': posts_html,
    }
    return HttpResponse(json.dumps(args),
                        content_type='application/json')
