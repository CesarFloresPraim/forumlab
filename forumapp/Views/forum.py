from django.shortcuts import render, redirect
from django.views.generic import TemplateView
from django.http import HttpResponse, JsonResponse
from forumapp.models import Post, Reply


class Forum(TemplateView):
    template_name = 'Forum/index.html'

    def get(self, request, *args, **kwargs):
        posts = Post.objects.all()
        posts_html = """
                   <div class="card text-center">
                       <div class="card-header">
                           Posts
                       </div>
           """

        for pst in posts:
            posts_html += """
               <div class="card-body">
                       <div class="card border-secondary mb-3">
                           <div class="card-header"><i class="fa fa-user"></i> User</div>
                           <div class="card-body text-secondary">
                               <div class="alert alert-secondary" role="alert" id="post-content-{1}">
                                   {0}
                               </div>
                               <button onclick="selectPost(this.id, true)" id="edit-post-{1}" type="button" class="btn btn-info"><i class="fa fa-pencil"></i></button>
                               <button onclick="deletePost(this.id)" id="delete-post-{1}" type="button" class="btn btn-danger"><i class="fa fa-trash"></i></button>
                               <button onclick="selectPost(this.id, true)" id="reply-post-{1}" type="button" class="btn btn-success"><i class="fa fa-paper-plane"></i></button>

                               <hr/>
               """.format(pst.text, pst.id)
            for rep in pst.reply_set.all():
                posts_html += """
                   <div>
                       <i class="fa fa-user"></i> User<p class="card-text ml-3" id="reply-content-{1}">{0}</p>
                   </div>
                   <button onclick="selectPost(this.id, false)" id="edit-rep-{1}" type="button" class="btn btn-info"><i class="fa fa-pencil"></i></button>
                   <button onclick="deletePost(this.id)" id="delete-rep-{1}" type="button" class="btn btn-danger"><i class="fa fa-trash"></i></button><hr/>
                   """.format(rep.text, rep.id)
            posts_html += """
                                       </div>
                           </div>
                       </div>
               """
        posts_html += """
                           <div class="card-footer text-muted">
                           Loaded a min ago ...
                       </div>
                   </div>
           """

        args = {
            'posts_html': posts_html,
        }
        return render(request, self.template_name, args)
