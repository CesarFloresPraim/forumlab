var ACTION_TYPE = '';
var TYPE_ID = '';
var TYPE = '';

function submitPost() {
    $("#post_post").submit();
    $("#parent_div_reply").hide();

}

function deletePost(id) {
    var prop = id.split("-");
    ACTION_TYPE = prop[0];
    TYPE = prop[1];
    TYPE_ID = prop[2];
    $("#post_post").submit();
}

function selectPost(id, post) {
    var prop = id.split("-");
    ACTION_TYPE = prop[0];
    TYPE = prop[1];
    TYPE_ID = prop[2];
    if (post) {
        $("#parent_div_reply").removeAttr('hidden');
        $("#editing_post").html($("#post-content-" + TYPE_ID).html());
    } else {
        $("#parent_div_reply").removeAttr('hidden');
        $("#editing_post").html($("#reply-content-" + TYPE_ID).html());
        $("#editing_post").html($("#reply-content-" + TYPE_ID).html());
    }


}

/*----Obiene el csrf_token del formulario----*/
function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie != '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) == (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}


$(document).ready(function () {

    /*----Peticiones con Ajax----*/
    $("#post_post").submit(function (e) {
        e.preventDefault();
        var csrftoken = getCookie('csrftoken');
        $.ajax({
            url: $(this).attr('action'),
            type: $(this).attr('method'),
            data: {
                'csrfmiddlewaretoken': csrftoken,
                'text': $("#new_post_text").val(),
                'action': ACTION_TYPE,
                'id': TYPE_ID,
                'type': TYPE
            },
            dataType: 'json',
            success: function (respuesta) {
                TYPE = 'new';
            },
            error: function (error_respuesta) {
                console.log(error_respuesta)
            }
        });

        setTimeout(function () {
            location.reload();
        }, 1500);

    });
});