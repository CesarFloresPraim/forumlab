from django.db import models

# Create your models here.
class Post(models.Model):
    text = models.CharField(max_length=100)

class Reply(models.Model):
    text = models.CharField(max_length=100)
    replies = models.ForeignKey(Post, on_delete=models.CASCADE)
